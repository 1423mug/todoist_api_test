#! /usr/bin/env python
# -*- coding: utf-8 -*-
import pytest
from key import token
import uuid


@pytest.fixture
def minimum_default_data():
    """
    Набор стандартных минимальных тестовых данных
    """
    return {
        "content": "Appointment with Maria2apitest",
        "due_string": "tomorrow at 12:00",
        "due_lang": "en",
        "priority": 4
    }


@pytest.fixture
def authorization_headers():
    """
    Набор стандартных заголовков для корректной авторизации
    """
    return {
        "content-type": "application/json",
        "Authorization": 'Bearer ' + token,
        "X-Request-Id": str(uuid.uuid4())
    }

@pytest.fixture(params=['0', '-50', 'afv', '66b3be1fbc24554db6e8ffe3d14870ac66940022'])
def data_with_wrong_token(authorization_headers, request):
    """
    Набор тестовых данных для теста с ошибочным ключом
    Ключ подменяется параметрами из фикстуры
    """
    data = authorization_headers.copy()
    error_bearer_token = "Bearer " + request.param
    data['Authorization'] = error_bearer_token
    return data
