# coding: utf-8
import pytest
import requests

url = "https://api.todoist.com/rest/v1/tasks"
# url = "http://httpbin.org/post" клевая штука для отладки. оставил что бы похвастаться


"""
Набор тесткейсов для прохождения собеседования в ZiMad
"""


def check_request_denied(resp):
    """
    проверка ответа на запрос с не верным ключом
    функция принимает на вход ответ от сервиса
    проверят ответ на соответсвие по статусу, коду
    и сообщению об ошибке
    """

    status = resp.text
    print('Текст ответа = ', status)
    assert status == 'Forbidden\n'
    assert resp.status_code == 403


def test_minimum_validate_request(minimum_default_data, authorization_headers):
    """
    тест
    запрос с минимальным набором корректных данных
    """

    resp = requests.post(url, json=minimum_default_data, headers=authorization_headers)

    print("Код ответа = ", resp.status_code)
    resp_json = resp.json()
    print(resp_json)
    task_id = resp_json["id"]
    print(task_id)
    assert task_id != 0
    assert resp.status_code == 200


def test_with_wrong_key(minimum_default_data, data_with_wrong_token):
    """
    тест
    негативный запрос с не корректным токеном
    """
    resp = requests.post(url, json=minimum_default_data, headers=data_with_wrong_token)
    check_request_denied(resp)
