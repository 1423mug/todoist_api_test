# Автоматизированное тестирование ToDoist 
Метод Create task

## Endpoint: [https://api.todoist.com/rest/v1/tasks](https://api.todoist.com/rest/v1/tasks)

Для параметризации тестов использованы фикстуры. Аналогично параметоризировать данные тестов можно используя декоратор @pytest.mark.parametrize. Выбор сделан в пользу фикстур так как не требуется запускать тесты с перекрестными наборами параметров (декартовым произведением параметров).

Отдельно вынесено значение токена в файл key.py для безопасности данных.
Фикстуры вынесены в отдельный файл conftest.py что бы максимально отделить логику работы автотестов от самих тестов.

>Для работы проекта необходим установленный Python 2.7.15 и pip 18.1
### Description:
Документация по тест-дизайну и поставленная задача вынесенны в отдельный файл на google docs
[https://docs.google.com/document/](https://docs.google.com/document/d/1dCxenwXPdtTXPgY1rRib18e8nJoDonz5QUsSzzt932U/edit)

### Установка:

1. Скопируйте проект из репозитория  

`git clone https://gitlab.com/1423mug/todoist_api_test.git`


2. Для запуска тестов установите пакет PyTest и Request
`sudo easy_install pip` (MacOS)

`pip install requests`

`pip install -U pytest`

### Запуск:

1. Перейдите в директорию с тестами

2. Измените значение токена в файле key.py
 
2. Выполните команду:

`pytest test.py`

или

`python -m pytest test.py`
  
### Сценарии:
#### Позитивные
**test_minimum_validate_request** - запрос с минимальным набором корректных данных

#### Негативные
**test_with_wrong_key** - набор негативных запросов с не корректным токеном